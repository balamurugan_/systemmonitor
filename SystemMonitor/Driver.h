/*++

Module Name:

    driver.h

Abstract:

    This file contains the driver definitions.

Environment:

    Kernel-mode Driver Framework

--*/

#include <ntddk.h>
#include <wdf.h>

#define NTSTRSAFE_LIB
#include <ntstrsafe.h>
#include <wdmsec.h> // for SDDLs

#include <initguid.h>

#include "device.h"
#include "queue.h"
#include "Trace.h"

EXTERN_C_START

//
// WDFDRIVER Events
//

DRIVER_INITIALIZE              DriverEntry;
EVT_WDF_DRIVER_DEVICE_ADD      SystemMonitorEvtDeviceAdd;
EVT_WDF_OBJECT_CONTEXT_CLEANUP SystemMonitorEvtDriverContextCleanup;
EVT_WDF_DRIVER_UNLOAD          SystemMonitorEvtDriverUnload;
DRIVER_UNLOAD                  SystemMonitorDriverUnload;

EXTERN_C_END
