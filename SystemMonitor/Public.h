/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that apps can find the device and talk to it.
//

DEFINE_GUID (GUID_DEVINTERFACE_SystemMonitor,
    0x34102a84,0xad0a,0x4120,0xbc,0xcc,0x8d,0x04,0xdf,0x82,0xb7,0xb5);
// {34102a84-ad0a-4120-bccc-8d04df82b7b5}
